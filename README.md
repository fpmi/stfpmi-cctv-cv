![build](https://gitlab.com/fpmi/stfpmi-cctv-cv/badges/dev/pipeline.svg)
![coverage](https://gitlab.com/fpmi/stfpmi-cctv-cv/badges/dev/coverage.svg)
# STFPMI-CCTV-CV
Проект для обработки видео с камер наблюдения и выдачи данных в виде API.

## Мотивация
Как мы знаем, в общежитиях ФПМИ есть общественные комнаты, по типу КДС, клуба и т.д., 
и студенты могут их бронировать на [mipt.tech](mipt.tech).
Но, к сожалению, часто случается, что студенты бронируют комнату, а потом не приходят.

Из-за этого комнаты остаются пустыми, а студенты, которые хотели бы воспользоваться ими, не могут этого сделать.

Для решения этой проблемы был создан этот проект.

## Как это работает
Для каждой из указанных комнат запускается процесс, 
который открывает видеопоток с камеры наблюдения и с помощью OpenCV находит разницу между 
двумя соседними кадрами в определенные моменты времени.
Из этой разницы выстраиваются временные отрезки активности.

При помощи обращения к API можно получить список таймлайнов активности для каждой из комнат, 
а также коэффициенты активности. (Об этом подробнее в разделе [Использование](#Использование))

По выдаче данных из API для любой брони можно определить, была ли она пустующей.

По результатам, ответственный за комнату может предпринимать меры к студентам, 
которые регулярно не приходят на свою бронь.


## Запуск

### Запуск в Docker

#### Подготовка
Необходимо создать файл `cctv-config` с перечислением ссылок на rtsp потоки в формате:
```
just_a_room /dev/video0 systemd 
another_room /dev/video1 systemd
```

#### Сборка
```bash
docker build -t stfpmi-cctv-cv .
```
#### Запуск
```bash
mkdir data
docker run \
--rm \
-e UVICORN_HOST=<host> \
-e UVICORN_PORT=<port> \
-v <absolute_path_to_cctv_config>:/app/cctv_config:ro \
-v $(pwd)/data:/app/data \
-p <port>:<port> \
stfpmi-cctv-cv \
--rooms='["just_a_room", "another_room"]'
```
или
```bash
cat >rooms.txt <<< '["just_a_room", "another_room"]'
```
```bash
mkdir data
docker run \
--rm \
-e UVICORN_HOST=<host> \
-e UVICORN_PORT=<port> \
-v <path_to_cctv_config>:/app/cctv_config:ro \
-v $(pwd)/data:/app/data \
-p <port>:<port> \
stfpmi-cctv-cv \
--rooms_file=rooms.txt
```

*ВАЖНО:* Путь до cctv_config файла должен быть абсолютным!

#### Запуск в debug режиме на MacOS \(experimental\)
P.s. Для остальных систем алгоритм действия может (и будет) отличаться.

##### Установка зависимостей
Установка XQuartz (если его нет на вашей машине)
```bash
brew cask install xquartz
```
P.s. после установки XQuartz нужно перезагрузить машину, чтобы все заработало.

##### Подготовка
Отключение контроля доступа для X11.
```bash
xhost +
```

Переопределяем значение переменной окружения ```DISPLAY```.
```bash
export DISPLAY=:0
```

##### Запуск
```bash
docker run \
--rm \
-e UVICORN_HOST=0.0.0.0 \
-e UVICORN_PORT=8000 \
-e DISPLAY=host.docker.internal:0 \
-v $(pwd)/cctv_config:/app/cctv_config:ro \
-v $(pwd)/data:/app/data \
-p 8000:8000 \
stfpmi-cctv-cv \
--rooms='["room"]' \
--debug
```


#### Запуск вручную
Запуск сервера
```bash
pip3 install -r requirements.txt
mkdir -p test/logs test/timelines
cat >test/cctv_config <<< 'just_a_room /dev/video0 systemd'
cd src
export SCCV_DIRECTORY_FOR_LOG=../test/logs
export SCCV_CCTV_CONFIG_FILE=../test/cctv_config
export SCCV_TIMELINE_FILE_TEMPLATE=../test/timelines/{}.json
export SCCV_MOTION_DETECTION='{"debug_mode": false}'
export SCCV_ROOMS_TO_WATCH='["just_a_room"]'
uvicorn --factory stfpmi_cctv_cv.application:Application.wsgi_from_env --host 127.0.0.1 --port 8000
```
## Переменные окружения
```SCCV_DIRECTORY_FOR_LOG``` --- директория для хранения логов

```SCCV_CCTV_CONFIG_FILE``` --- путь к конфигу с потоками камер

```SCCV_TIMELINE_FILE_TEMPLATE``` --- шаблон файла для хранения временных отрезков

```SCCV_MOTION_DETECTION``` --- параметры для CV воркера

```SCCV_ROOMS_TO_WATCH``` --- список комнат, для которых нужно запустить воркеры

### Пример запуска
Скопируйте строки ниже в терминал.
```bash
git clone git@gitlab.com:fpmi/stfpmi-cctv-cv.git
cd stfpmi-cctv-cv
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
mkdir -p test/logs test/timelines
cat >test/cctv_config <<< 'vid rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4 systemd'
cd src
export SCCV_DIRECTORY_FOR_LOG=../test/logs
export SCCV_CCTV_CONFIG_FILE=../test/cctv_config
export SCCV_TIMELINE_FILE_TEMPLATE=../test/timelines/{}.json
export SCCV_MOTION_DETECTION='{"debug_mode": true}'
export SCCV_ROOMS_TO_WATCH='["vid"]'
uvicorn --factory stfpmi_cctv_cv.application:Application.wsgi_from_env --host 127.0.0.1 --port 8000
```
Если все хорошо, вам должно открыться дебаг-окно. 
Также вы можете попробовать поотправлять [запросы](http://127.0.0.1:8000/docs).

## Использование

Проверка работоспособности API
```bash
curl localhost:8000/health
```

Получение таймлайна
```bash
curl localhost:8000/timeline/just_a_room
```

Получение таймлайна с фильтрацией по времени
```bash
curl localhost:8000/timeline/just_a_room?start=unix-time&end=unix-time
```

Получение коэффициента загруженности
```bash
curl localhost:8000/activity/just_a_room?start=unix-time&end=unix-time
```
