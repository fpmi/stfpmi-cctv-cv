FROM ubuntu:22.04

# Install depndencies
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python3-dev \
    ffmpeg \
    libsm6 \
    libxext6 \
    x11-apps

RUN mkdir /app
# Install python depndencies
COPY requirements.txt /app/
RUN pip3 install --upgrade pip
RUN pip3 install -r /app/requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app
WORKDIR /app/src


# Set environment variables
ENV SCCV_DIRECTORY_FOR_LOG=../data/logs
ENV SCCV_CCTV_CONFIG_FILE=/app/cctv_config
ENV SCCV_TIMELINE_FILE_TEMPLATE=../data/timelines/timeline_{}.json
ENV DISPLAY=host.docker.internal:0

ENTRYPOINT ["../run.sh"]