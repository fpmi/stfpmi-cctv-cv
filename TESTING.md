## Shell 1

```
mkdir -p test/logs test/timelines
cat >test/cctv_config <<< 'just_a_room /dev/video0 systemd'
cd src
export SCCV_DIRECTORY_FOR_LOG=../test/logs
export SCCV_CCTV_CONFIG_FILE=../test/cctv_config
export SCCV_TIMELINE_FILE_TEMPLATE=../test/timelines/{}.json
export SCCV_MOTION_DETECTION='{"debug_mode": true}'
export SCCV_ROOMS_TO_WATCH='["just_a_room"]'
uvicorn --factory stfpmi_cctv_cv.application:Application.wsgi_from_env --host 127.0.0.1 --port 8000
```

Check cv2 debug window

## Shell 2

```
curl localhost:8000/health
```

Check code 200, body "OK"

```
curl localhost:8000/timeline/just_a_room
```

Check code 200, non-empty json array

## Back to shell 1

`Ctrl+C`

Check immediate exit

Check worker log in `test/logs/cctv-cv_just_a_room.log`
