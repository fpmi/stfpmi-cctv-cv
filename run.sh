#!/usr/bin/env bash

HELP_MESSAGE="Usage: ./run.sh [--debug] [--rooms_file=rooms.txt] [--rooms=[\"room1\", \"room2\"]]"
HELP_MESSAGE+="

  --debug: enable debug mode
  --rooms_file: path to file containing rooms to watch (has priority over --rooms)
  --rooms: rooms to watch

"
HELP_MESSAGE+="

  Example:
  ./run.sh --debug --rooms_file=rooms.txt
  ./run.sh --debug --rooms=[\"room1\", \"room2\"]
"


# Parse arguments
DEBUG=false
ROOMS_FROM_FILE=false
for i in "$@"; do
  case $i in
  --debug)
    DEBUG=true
    shift # past argument=value
    ;;
  --rooms_file=*)
    ROOMS_FILE="${i#*=}"
    ROOMS_FROM_FILE=true
    shift # past argument=value
    ;;
  --rooms=*)
    ROOMS="${i#*=}"
    shift # past argument=value
    ;;
  *)
    # unknown option
    ;;
  esac
done

# if rooms not from file and not specified, exit
if [ "$ROOMS_FROM_FILE" = false ] && [ -z "$ROOMS" ]; then
  echo "No rooms specified"
  printf "\n%s" "$HELP_MESSAGE"
  exit 1
fi

# check if rooms file exists
if [ "$ROOMS_FROM_FILE" = true ] && [ ! -f "$ROOMS_FILE" ]; then
  echo "Rooms file does not exist"
  printf "\n%s" "$HELP_MESSAGE"
  exit 1
fi

# if rooms from file, read rooms from file.
# format: '["room1", "room2", "room3"]'
if [ "$ROOMS_FROM_FILE" = true ]; then
  ROOMS=$(cat "${ROOMS_FILE}")
fi

# if debug
if [ "$DEBUG" = true ]; then
  export SCCV_MOTION_DETECTION='{"debug_mode": true}'
else
  export SCCV_MOTION_DETECTION='{"debug_mode": false}'
fi

echo $ROOMS
export SCCV_ROOMS_TO_WATCH=$ROOMS

mkdir -p /app/data/logs
mkdir -p /app/data/timelines

# run
uvicorn --factory stfpmi_cctv_cv.application:Application.wsgi_from_env

