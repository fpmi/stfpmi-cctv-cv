from stfpmi_cctv_cv import motion_detection, storage
from stfpmi_cctv_cv.config import MotionDetectionSettings
import subprocess as sp
import pytest
import os
import time


class Logger:
    def warning(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def debug(self, *args, **kwargs):
        pass

    def exception(self, *args, **kwargs):
        raise Exception("Test exception")


@pytest.fixture
def test_dir():
    return sp.check_output('mktemp -d', shell=True).decode('utf-8').strip()


@pytest.fixture
def test_file(test_dir):
    return os.path.join(test_dir, 'test.json')


def test_motion_detect(test_file):
    test_rtsp = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4"
    stg = storage.FileTimelineStorage(test_file, Logger())

    md = motion_detection.MotionDetector(
        MotionDetectionSettings(),
        test_rtsp,
        stg,
        Logger(),
    )

    curr_time = time.time()

    def fake_canceled():
        return time.time() - curr_time > 5

    md.run(fake_canceled)

    assert stg.read_timeline() is not None, "File should exist"
    assert len(stg.read_timeline()) > 0, "There should be at least one entry"
