from stfpmi_cctv_cv import worker
import pytest
import os

running_flag = False


class MotionDetector:
    def run(self, cancelled):
        global running_flag
        running_flag = True

    def reset(self):
        pass

    def __init__(self, *args, **kwargs):
        pass

    def __del__(self):
        pass


class Logger:
    def warning(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def debug(self, *args, **kwargs):
        pass

    def exception(self, *args, **kwargs):
        raise Exception("Test exception")


def test_worker():
    detector = MotionDetector()
    parent_pid = os.getpid()
    logger = Logger()
    w = worker.Worker(detector, parent_pid, logger)
    # run worker in a separate thread

    assert running_flag is False, "Should not be running yet"

    w.run()

    assert w._cancelled() is False, "Should not be cancelled by signal"
    assert running_flag is True, "Should be running now"
