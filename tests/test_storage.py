from stfpmi_cctv_cv import storage
import pytest
import os
import shutil
import subprocess as sp
import time


@pytest.fixture
def test_dir():
    return sp.check_output('mktemp -d', shell=True).decode('utf-8').strip()


@pytest.fixture
def test_file(test_dir):
    return os.path.join(test_dir, 'test.json')


class Logger:
    def warning(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def debug(self, *args, **kwargs):
        pass


def test_file_timeline_storage(test_file):
    if os.path.exists(test_file):
        os.remove(test_file)

    stg = storage.FileTimelineStorage(test_file, Logger())
    stg.open_timeline_entry()
    assert os.path.exists(test_file), "File should exist"
    assert stg.read_timeline()[0][1] is None, "End time should be None"
    assert stg.read_timeline()[0][0] is not None, "Start time should not be None"

    time.sleep(1)
    stg.finalize_timeline_entry()
    assert stg.read_timeline()[0][1] is not None, "End time should not be None"
    assert stg.read_timeline()[0][0] is not None, "Start time should not be None"
    assert stg.read_timeline()[0][1] - stg.read_timeline()[0][0] > 0.5, "Time should have passed"
    assert stg.read_timeline()[0][1] - stg.read_timeline()[0][0] < 1.5, "No more than 1.5s should have passed"

    assert stg.read_timeline_for_segment(0, 1) == [], "There is no segment should be"
    assert stg.read_timeline_for_segment(0, stg.read_timeline()[0][1] + 1) != [], "There is a segment should be"

    stg.open_timeline_entry()
    stg.sanitize_last_timeline_entry()
    assert len(stg.read_timeline()) == 1, "Should have only one entry, cause the last one was sanitized"
    assert stg.read_timeline()[-1][1] is not None, "End time should not be None, cause the last one was sanitized"
    assert stg.read_timeline()[-1][0] is not None, "Start time should not be None, cause the last one was sanitized"

    os.remove(test_file)
    assert stg.read_timeline() is None, "File not exist, so read_timeline should return None"


