from stfpmi_cctv_cv import process_manager, application, motion_detection, storage
from stfpmi_cctv_cv.config import MotionDetectionSettings, ApplicationConfig
from pathlib import Path
import subprocess as sp
import pytest
import os
import time


class Logger:
    def warning(self, *args, **kwargs):
        pass

    def info(self, *args, **kwargs):
        pass

    def debug(self, *args, **kwargs):
        pass

    def exception(self, *args, **kwargs):
        raise Exception("Test exception")


@pytest.fixture
def test_dir():
    return sp.check_output('mktemp -d', shell=True).decode('utf-8').strip()


@pytest.fixture
def test_file(test_dir):
    return os.path.join(test_dir, 'test.json')


def test_dumb_application(test_file):
    app = application.Application(
        ApplicationConfig()
    )
    app.start()


def test_app_with_fake_room(test_dir):
    app1 = application.Application(
        ApplicationConfig(
            room_urls={"room": "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mp4"},
            rooms_to_watch=["room"],
            directory_for_log=test_dir,
        )
    )
    app1.start()

    Path("timeline_room.json").touch()
    with open("timeline_room.json", "w") as f:
        f.write("[]")
    stg = app1.timeline_storage("room")
    time.sleep(2)
    app1.stop()
    os.remove("timeline_room.json")
