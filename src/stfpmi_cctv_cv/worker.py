import cProfile
import signal
import sys
from datetime import datetime, timedelta
from logging import Logger

import psutil

from stfpmi_cctv_cv.motion_detection import MotionDetector


class Worker:
    _STOP_SIGNALS = [
        signal.SIGTERM,
        signal.SIGINT,
    ]

    # Limit calls to psutil
    # TODO make configurable
    _PARENT_CHECK_INTERVAL = timedelta(seconds=5)

    def __init__(self, detector: MotionDetector, parent_pid: int, logger: Logger):
        self.detector = detector
        self.parent_pid = parent_pid
        self.logger = logger
        self._last_parent_check = datetime.now()
        self._cancelled_by_signal = False

    def run(self) -> None:
        try:
            for signo in self._STOP_SIGNALS:
                signal.signal(signo, self._on_stop_signal)

            self.logger.info("Started")
            self.detector.run(self._cancelled)
            self.logger.info("Finished")
        except Exception:
            self.logger.exception("FATAL: unhandled exception")

    def _cancelled(self) -> bool:
        now = datetime.now()
        if now >= self._last_parent_check + self._PARENT_CHECK_INTERVAL:
            try:
                psutil.Process(self.parent_pid)
                self._last_parent_check = now
            except psutil.NoSuchProcess:
                self.logger.fatal("Parent process dead, exiting")
                return True

        return self._cancelled_by_signal

    def _on_stop_signal(self, signo: int, _stack) -> None:
        self._cancelled_by_signal = True
        self.logger.info("Cancelled by signal %s", signal.Signals(signo).name)
