import json
import logging
import typing as tp
from logging.handlers import TimedRotatingFileHandler

import psutil
from fastapi import FastAPI

from stfpmi_cctv_cv.config import ApplicationConfig
from stfpmi_cctv_cv.motion_detection import MotionDetector
from stfpmi_cctv_cv.process_manager import ProcessManager
from stfpmi_cctv_cv.routes import root_router
from stfpmi_cctv_cv.storage import (
    FileTimelineStorage,
    TimelineStorageAbc,
)
from stfpmi_cctv_cv.worker import Worker


def make_worker(config: ApplicationConfig, room_name: str, room_url: str, this_pid: int) -> Worker:
    log_path = f"{config.directory_for_log}/cctv-cv_{room_name}.log"
    handler = TimedRotatingFileHandler(log_path, when="midnight")
    handler.setLevel(getattr(logging, config.log_level.value))
    handler.setFormatter(logging.Formatter(config.log_format))
    logger = logging.getLogger(f"worker_{room_name}")
    logger.addHandler(handler)
    logger.propagate = False

    storage_filename = config.timeline_file_template.format(room_name)
    storage = FileTimelineStorage(storage_filename, logger.getChild("storage"))

    detector = MotionDetector(config.motion_detection, room_url, storage, logger.getChild("detector"))

    return Worker(detector, this_pid, logger)


class Application:
    def __init__(self, config: ApplicationConfig):
        logging.basicConfig(
            level=getattr(logging, config.log_level.value),
            format=config.log_format,
        )
        self.logger = logging.getLogger("application")

        self.config = config
        self._process_manager: tp.Optional[ProcessManager] = None
        self._api = FastAPI(
            on_startup=[self.start],
            on_shutdown=[self.stop],
        )
        self._api.include_router(root_router)
        self._api.application_ref = self  # To access from api handles

    def timeline_storage(self, room_name) -> TimelineStorageAbc:
        filename = self.config.timeline_file_template.format(room_name)
        return FileTimelineStorage(filename, self.logger.getChild(f"storage_{room_name}"))

    def start(self) -> None:
        self.logger.info("Rooms in watchlist: %s", " ".join(self.config.room_urls.keys()))
        assert self._process_manager is None

        this_pid = psutil.Process().pid
        workers = {}
        for name, url in self.config.room_urls.items():
            workers[name] = make_worker(self.config, name, url, this_pid)

        self._process_manager = ProcessManager(self.logger.getChild("process_manager"))
        self._process_manager.start(workers)

    def stop(self) -> None:
        assert self._process_manager is not None
        self._process_manager.join(self.config.exit_join_timeout_sec)
        self._process_manager = None

    @classmethod
    def from_env(cls) -> 'Application':
        config = ApplicationConfig.from_cctv_config()
        return cls(config)

    @classmethod
    def wsgi_from_env(cls) -> FastAPI:
        application = cls.from_env()
        return application._api
