import json
import os
import time
import bisect
from math import inf
import typing as tp
from abc import ABC, abstractmethod
from logging import Logger

Timeline = tp.List[tp.Tuple[float, tp.Optional[float]]]


class TimelineStorageAbc(ABC):
    @abstractmethod
    def open_timeline_entry(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def finalize_timeline_entry(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def sanitize_last_timeline_entry(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def read_timeline_for_segment(self, start: float, end: float) -> tp.Optional[Timeline]:
        raise NotImplementedError()

    @abstractmethod
    def read_timeline(self) -> tp.Optional[Timeline]:
        raise NotImplementedError()

    @abstractmethod
    def _write_timeline(self, timeline: Timeline) -> None:
        raise NotImplementedError()


class FileTimelineStorage(TimelineStorageAbc):
    """Tries to improve consistency, still best-effort. At least needs more dir syncs."""

    def __init__(self, path: str, logger: Logger):
        self.path = path
        self.logger = logger

    def read_timeline(self) -> tp.Optional[Timeline]:
        if os.path.exists(self.path):
            with open(self.path, "r") as f:
                return json.load(f)
        else:
            self.logger.warning("File does not exist: %s", self.path)
            return None

    def _write_timeline(self, timeline: Timeline) -> None:
        # Missing some dir syncs
        tmp_path = self.path + ".tmp"

        with open(tmp_path, "w") as f:
            json.dump(timeline, f)

        # Atomic in the same way as rename(2)
        os.replace(tmp_path, self.path)

    def open_timeline_entry(self) -> None:
        timeline = self.read_timeline() or []
        timeline.append((time.time(), None))
        self._write_timeline(timeline)

    def finalize_timeline_entry(self) -> None:
        timeline = self.read_timeline()
        timeline[-1] = (timeline[-1][0], time.time())
        self._write_timeline(timeline)

    def sanitize_last_timeline_entry(self) -> None:
        timeline = self.read_timeline()
        if timeline and timeline[-1][1] is None:
            timeline.pop()
            self.logger.warning("! Timeline was not closed properly. Deleting last entry.")
            self._write_timeline(timeline)

    def read_timeline_for_segment(self, start: float, end: float) -> tp.Optional[Timeline]:
        timeline_raw = self.read_timeline()

        if timeline_raw is None:
            return None

        if start > end:
            return None

        timeline = list(timeline_raw)
        if len(timeline) != 0:
            if timeline[-1][1] is None:
                timeline[-1] = (timeline[-1][0], time.time())

        start_index = bisect.bisect_left(timeline, start, key=lambda x: x[0])
        # include last seq (replace none with end_time in request)
        end_index = bisect.bisect_right(timeline, end, key=lambda x: time.time() - 1 if x[1] is None else x[1])

        cropped_timeline = timeline[start_index:end_index]
        ex_left = timeline[start_index - 1] if start_index > 0 else None
        ex_right = timeline[end_index] if end_index < len(timeline) else None

        extend_left = []
        extend_right = []

        if len(cropped_timeline) == 0 and (ex_right is not None):
            if (end_index == (start_index - 1)) and (start > ex_right[0]) and (end < ex_right[1]):
                return [(start, end)]

        if ex_left:
            if start < ex_left[1]:
                extend_left.append((start, ex_left[1]))

        if ex_right:
            if end > ex_right[0]:
                extend_right.append((ex_right[0], end))

        return extend_left + timeline[start_index:end_index] + extend_right
