from fastapi import (
    APIRouter,
    Request,
)
import time


root_router = APIRouter()


@root_router.get("/health")
def health_check():
    return {"status": "ok"}


@root_router.get("/timeline/{room_name}")
def timeline(request: Request, room_name: str, start: int = None, end: int = None):
    application = request.app.application_ref

    if start is None:
        start = 0
    if end is None:
        end = time.time()

    return {
        "status": "ok",
        "response": application.timeline_storage(room_name).read_timeline_for_segment(start, end)
    }


@root_router.get("/activity/{room_name}")
def activity_for_segment(request: Request, room_name: str, start: int = None, end: int = None):
    application = request.app.application_ref

    if start is None:
        start = 0
    if end is None:
        end = time.time()

    timeline_data = application.timeline_storage(room_name).read_timeline_for_segment(start, end)
    timeline_end = timeline_data[-1][1] or time.time()
    activity_coefficient = sum((x[1] - x[0]) for x in timeline_data)
    activity_coefficient /= end - start
    activity_coefficient = round(activity_coefficient, 2)

    return {
        "status": "ok",
        "response": {
            "activity_coefficient": activity_coefficient,
        }
    }
