import signal

from stfpmi_cctv_cv.application import Application


def main():
    application = Application.from_env()
    application.start()
    signal.sigwait({ signal.SIGTERM, signal.SIGINT })
    application.stop()


if __name__ == "__main__":
    main()
