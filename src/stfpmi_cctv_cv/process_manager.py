import typing as tp
from logging import Logger
from multiprocessing import Process

from stfpmi_cctv_cv.worker import Worker


class ProcessManager:
    def __init__(self, logger: Logger):
        self.logger = logger
        self._processes: tp.Optional[tp.Dict[str, Process]] = None

    def start(self, workers: tp.Dict[str, Worker]) -> None:
        assert self._processes is None
        self._processes = {}
        for room_name, worker in workers.items():
            p = Process(target=worker.run)
            p.start()
            self.logger.info("Process for room %s started, pid %d", room_name, p.pid)
            self._processes[room_name] = p

        self.logger.info("All processes started")

    def join(self, timeout_sec: tp.Optional[float]) -> None:
        assert self._processes is not None
        for room_name, p in self._processes.items():
            p.terminate()

        for room_name, p in self._processes.items():
            p.join(timeout_sec)
            if p.is_alive():
                p.kill()
                self.logger.warning("Process join for room %s timed out, killed", room_name)

        self._processes = None
        self.logger.info("All processes finished")
