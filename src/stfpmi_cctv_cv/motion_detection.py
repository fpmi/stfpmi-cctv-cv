import time
import typing as tp
from logging import Logger

import cv2
import imutils
import numpy as np

from stfpmi_cctv_cv.config import MotionDetectionSettings
from stfpmi_cctv_cv.storage import TimelineStorageAbc


class DebugInfo:
    def __init__(self):
        self.bounding_boxes = []
        self.text = "Unoccupied"
        self.frame_bgr = None
        self.frame_delta_bgr = None


class MotionDetector:
    _DEBUG_FONT = cv2.FONT_HERSHEY_SIMPLEX

    def __init__(
        self,
        settings: MotionDetectionSettings,
        stream_url: str,
        storage: TimelineStorageAbc,
        logger: Logger
    ):
        self.settings = settings
        self.stream_url = stream_url
        self.storage = storage
        self.logger = logger

        self.capture = None
        self.reset()

    def reset(self) -> None:
        if self.capture is not None:
            if self.settings.debug_mode:
                cv2.destroyAllWindows()

            self.capture.release()

        self.capture = None

        self.base_frame = None  # Frame to calculate diff against (grayscale)
        self.prev_frame = None  # Previous processed frame (grayscale)
        self.base_frame_ttl = self.settings.frames_to_persist  # Frames until base_frame update

        self.movement_persistent_counter = 0  # Counter for persistent movement
        self.movement_persistent_flag = False

        self.debug_info = None

    def run(self, cancelled: tp.Callable[[], bool]) -> None:
        assert self.capture is None
        try:
            self.capture = cv2.VideoCapture(self.stream_url)

            # Check if last saved timeline entry is open (end_time is None)
            self.storage.sanitize_last_timeline_entry()

            while not cancelled():
                self.debug_info = DebugInfo()
                self.cycle()
                if self.settings.debug_mode:
                    self.update_debug_window()
                    cv2.waitKey(self.settings.inter_frame_sleep_ms or 1)
                else:
                    if self.settings.inter_frame_sleep_ms is not None:
                        time.sleep(self.settings.inter_frame_sleep_ms / 1000)

            self.logger.info("Cancelled, exiting")
        finally:
            self.reset()

    def cycle(self) -> None:
        is_movement = self.transient_movement()
        if is_movement:
            if not self.movement_persistent_flag:  # If movement was not persistent before, start counting
                self.storage.open_timeline_entry()
                self.logger.info("Movement detected")

            self.movement_persistent_flag = True
            self.movement_persistent_counter = self.settings.movement_detected_persistence

        if self.movement_persistent_counter > 0:  # If movement was persistent, count down
            self.debug_info.text = f"Movement Detected {self.movement_persistent_counter}"
            self.movement_persistent_counter -= 1

            if self.movement_persistent_counter <= 0:  # Stop counting if movement is not persistent anymore
                self.movement_persistent_flag = False
                self.storage.finalize_timeline_entry()
        else:
            self.debug_info.text = "No Movement Detected"

    def transient_movement(self) -> bool:
        ret, frame = self.capture.read()
        if not ret:
            self.logger.warning(
                "Can't receive frame for room. Trying again in %d seconds.",
                self.settings.read_retry_delay_sec,
            )
            time.sleep(self.settings.read_retry_delay_sec)
            return False

        frame = imutils.resize(frame, width=750)  # Resize frame for better performance
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # Convert frame to grayscale
        gray = cv2.GaussianBlur(gray, (21, 21), 0)  # Blur frame to reduce noise

        if self.settings.debug_mode:
            self.debug_info.frame_bgr = frame
        if self.base_frame is None:
            self.base_frame = gray
        self.base_frame_ttl -= 1

        # Update frame to compare to every {FRAMES_TO_PERSIST} frames
        if self.base_frame_ttl <= 0:
            self.base_frame = self.prev_frame
            self.base_frame_ttl = self.settings.frames_to_persist

        self.prev_frame = gray
        frame_delta = cv2.absdiff(self.base_frame, self.prev_frame)  # Calculate difference between first and next frame
        thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]  # Threshold the delta image
        if self.settings.debug_mode:
            self.debug_info.frame_delta_bgr = cv2.cvtColor(frame_delta, cv2.COLOR_GRAY2BGR)

        # Dilate the thresholded image to fill in holes
        thresh = cv2.dilate(thresh, None, iterations=2)
        # Find contours on thresholded image
        cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        any_contour_above_thresh = False
        for c in cnts:
            # If contour is big enough, count it as movement
            if cv2.contourArea(c) > self.settings.min_size_for_movement:
                any_contour_above_thresh = True
                if self.settings.debug_mode:
                    self.debug_info.bounding_boxes.append(cv2.boundingRect(c))
                else:
                    # Short path: no need to look at other contours without debug mode
                    return True

        return any_contour_above_thresh

    def update_debug_window(self):
        for (x, y, w, h) in self.debug_info.bounding_boxes:
            cv2.rectangle(
                self.debug_info.frame_bgr,
                (x, y),
                (x + w, y + h),
                (0, 255, 0),
                2,
            )
        cv2.putText(
            self.debug_info.frame_bgr,
            self.debug_info.text,
            (10, 35),
            self._DEBUG_FONT,
            0.75,
            (255, 255, 255),
            2,
            cv2.LINE_AA,
        )
        cv2.imshow(
            self.stream_url,
            np.hstack((self.debug_info.frame_delta_bgr, self.debug_info.frame_bgr)),
        )
