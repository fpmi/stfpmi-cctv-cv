import logging
import typing as tp
from enum import Enum

from pydantic import BaseSettings


class MotionDetectionSettings(BaseSettings):
    # Delay between retries to read frame from camera
    read_retry_delay_sec = 60

    # Number of frames to pass before changing the frame to compare the current frame against
    frames_to_persist = 10

    # Minimum boxed area for a detected motion to count as actual motion
    # Use to filter out noise or small objects
    min_size_for_movement = 100

    # Minimum length of time when no motion is detected it should take
    # (in program cycles) for the program to declare that there is no movement
    movement_detected_persistence = 400

    # Used to limit frame rate
    # None -> no sleep between frames
    inter_frame_sleep_ms: tp.Optional[int] = None

    debug_mode = False


class Loglevel(Enum):
    CRITICAL = 'CRITICAL'
    ERROR = 'ERROR'
    WARNING = 'WARNING'
    INFO = 'INFO'
    DEBUG = 'DEBUG'


class ApplicationConfig(BaseSettings):
    log_level = Loglevel.INFO
    log_format = "[%(asctime)s] [%(levelname)8s] [%(name)s] %(message)s"

    directory_for_log = "logs/"
    cctv_config_file = "cam_config"
    timeline_file_template = "timeline_{}.json"

    rooms_to_watch: tp.List[str] = []  # room names
    room_urls: tp.Dict[str, str] = {}  # room name -> stream url

    motion_detection: MotionDetectionSettings = {}

    exit_join_timeout_sec = 5

    @classmethod
    def from_cctv_config(cls) -> 'ApplicationConfig':
        config = cls()
        config.room_urls = {}
        with open(config.cctv_config_file, "r") as cctv_config:
            for line in cctv_config:
                fields = list(filter(None, line.split(" ")))  # Remove tracing spaces and split by spaces
                room_name, room_url, _ = fields
                if room_name in config.rooms_to_watch:
                    config.room_urls[room_name] = room_url

        return config

    class Config:
        env_prefix = 'SCCV_'
        case_sensitive = False
